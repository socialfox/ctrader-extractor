from math import radians
import numpy as np                # installed with matplotlib
import matplotlib.pyplot as plt

#def main():
#   x = np.arange(0, radians(1800), radians(12))
#   plt.plot(x, np.cos(x), 'b')
#   plt.show()

def getdata(filename):
    RSI_MAX_LIST = []
    BARS_TO_HIGH_LIST = []
    with open(filename) as file:
        for line in file:
            i=0
            for char in line[9:]:
                if (char >='0' and char <='9') or char == ".":
                    i+=1
                else:
                    break
            j=0
            for char in reversed(line[:-1]):
                if (char >='0' and char <='9'):
                    j+=1
                else:
                    break
            #print(line[9:9+i])
            #RSI_MAX_LIST.append((line[9:9+i]))
            #BARS_TO_HIGH_LIST.append((line[-1-j:-1]))
            RSI_MAX_LIST.append(float(line[9:9+i]))
            BARS_TO_HIGH_LIST.append(int(line[-2]))
    return [RSI_MAX_LIST,BARS_TO_HIGH_LIST]
            
        



def main():
    #x=range(1,10)
    #y=range(1,10)
    #plt.plot(x,y,'o')
    #plt.ylabel("y numbers")
    #plt.xlabel("x numbers")
    #plt.show()

    filename = "Data/XAGUSD-Daily.txt"
    
    [RSI_MAX_LIST,BARS_TO_HIGH_LIST]=getdata(filename)
    print()
    print()

    plt.plot(RSI_MAX_LIST,BARS_TO_HIGH_LIST,'o')
    plt.ylabel("RSI Max")
    plt.xlabel("Bars to high")
    plt.show()

main()