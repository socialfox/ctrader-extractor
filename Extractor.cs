using System;
using System.Linq;
using cAlgo.API;
using cAlgo.API.Indicators;
using cAlgo.API.Internals;
using cAlgo.Indicators;
using System.IO;

namespace cAlgo.Robots
{
    [Robot(TimeZone = TimeZones.UTC, AccessRights = AccessRights.FullAccess)]
    public class NewcBot : Robot
    {
        //[Parameter(DefaultValue = 0.0)]
        //public double Parameter { get; set; }

        [Parameter("RSI Width", DefaultValue = 9)]
        public int RSIWidth { get; set; }

        [Parameter("Bars to check", DefaultValue = 2000)]
        public int BarsToCheck { get; set; }

        //indicator variables

        private RelativeStrengthIndex rsi;

        public bool Tracking;

        public int RSIMaximumBar = 0;
        public int PriceMaximumBar = 0;

        public double RSIMaximumValue = 0;
        public double PriceMaximumValue = 0;
        public StreamWriter outputFile;


        protected override void OnStart()
        {
            rsi = Indicators.RelativeStrengthIndex(Bars.ClosePrices, RSIWidth);
            // Put your initialization logic here
            
            string filename = "D:/Programming/CTrader-extractor/Data/"+Symbol.Name+"-"+TimeFrame+".txt";
            outputFile = new StreamWriter(filename, true);
            //outputFile.WriteLine("TEST!");
            
            //Print(filename);

            if (rsi.Result.Last(1) > 75)
            {
                Tracking = true;
                RSIMaximumBar = 1;
                PriceMaximumBar = 1;
                RSIMaximumValue = rsi.Result.Last(1);
                PriceMaximumValue = Bars.Last(1).High;
                //Print("RSIShort -> True");
            }
            else
                Tracking = false;
                
        }

        protected override void OnBar()
        {
           
            
            if ((rsi.Result.Last(1) > 75)&&(!Tracking))
            {
                Tracking = true;
                RSIMaximumBar = 1;
                PriceMaximumBar = 1;
                RSIMaximumValue = rsi.Result.Last(1);
                PriceMaximumValue = Bars.Last(1).High;
            }
            
            if ((rsi.Result.Last(1) < 40)&&Tracking)
            {
                Tracking = false;
                int diff = RSIMaximumBar - PriceMaximumBar;
                Print("RSIMax = {0} and BarsToPriceMax = {1} \n", RSIMaximumValue, diff);
                
                outputFile.WriteLine("RSIMax = {0} and BarsToPriceMax = {1}", RSIMaximumValue, diff);
                
            }
            else
            if (Tracking)
            {
                if (RSIMaximumValue<rsi.Result.Last(1))
                {
                    RSIMaximumValue = rsi.Result.Last(1);
                    RSIMaximumBar = 1;
                }
                else
                    RSIMaximumBar++;
                
                if (PriceMaximumValue<Bars.Last(1).High)
                {
                    PriceMaximumValue = Bars.Last(1).High;
                    PriceMaximumBar = 1;
                }
                else
                    PriceMaximumBar++;
                
            }
            
            


        }




        protected override void OnStop()
        {
            outputFile.Close();
            // Put your deinitialization logic here
        }
    }
}

